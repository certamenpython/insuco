from django import forms
from .models import Asignatura

class AsigModelForm(forms.ModelForm):
    class Meta:
        modelo = Asignatura
        campos = ["nombre_asignatura","nombre_carrera","semestre","fecha_registro"]

class AsigForm(forms.Form):
    nombre_asignatura = forms.CharField(max_length=100)
    nombre_carrera = forms.CharField(max_length=100)
    semestre = forms.IntegerField()
    fecha_registro = forms.DateTimeField()
