from django.shortcuts import render
from .forms import AsigForm
from .models import Asignatura

def inicio(request):
    form = AsigForm(request.POST or None)

    if form.is_valid():
        form_data = form.cleaned_data
        nombre_asignatura2 = form_data.get("nombre_asignatura")
        nombre_carrera2 = form_data.get("nombre_carrera")
        semestre2 = form_data.get("semestre")
        fecha_registro2 = form_data.get("fecha_registro")
        objeto = Asignatura.objects.create(nombre_asignatura=nombre_asignatura2,nombre_carrera=nombre_carrera2,semestre=semestre2,fecha_registro=fecha_registro2)




    contexto = {
        "el_formulario" : form
    }


    return render(request,"asignatura.html",contexto)



# Create your views here.
