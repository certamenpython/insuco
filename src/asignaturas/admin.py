from django.contrib import admin
from asignaturas.models import *
from .forms import AsigForm,AsigModelForm

class AdminAsignatura(admin.ModelAdmin):
    list_display = ["nombre_asignatura","nombre_carrera","semestre","fecha_registro"]
    form = AsigModelForm
    list_filter = ["fecha_registro"]
    list_editable = ["semestre"]
    search_fields = ["nombre_asignatura","nombre_carrera"]

    class Meta:
        model = Asignatura




admin.site.register(Asignatura,AdminAsignatura)

# Register your models here.
