from django.db import models
from carrera.models import Carrera

class Asignatura(models.Model):
    id_asignatura = models.AutoField(primary_key=True,unique=True)
    nombre_asignatura = models.CharField(max_length=100)
    nombre_carrera = models.ForeignKey(Carrera,blank=True,null=True)
    semestre = models.PositiveIntegerField()
    fecha_registro = models.DateTimeField(auto_now=False)

    def __str__(self):
        return self.nombre_asignatura

# Create your models here.
