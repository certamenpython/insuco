from django.contrib import admin
from carrera.models import *
from .forms import CarreForm,CarreModelForm

class AdminCarrera(admin.ModelAdmin):
    list_display = ["nombre_carrera","descripcion","num_estudiantes","nombre_facultad","fecha_apertura"]
    form = CarreModelForm
    list_filter = ["nombre_facultad"]
    list_editable = ["num_estudiantes"]
    search_fields = ["nombre_carrera","nombre_facultad"]
    class Meta:
        model = Carrera



admin.site.register(Carrera,AdminCarrera)



# Register your models here.
