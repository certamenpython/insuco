from django import forms
from .models import Carrera


class CarreModelForm(forms.ModelForm):
    class Meta:
        modelo = Carrera
        campos = ["nombre_carrera","descripcion","num_estudiantes","nombre_facultad","fecha_apertura"]



class CarreForm(forms.Form):
    nombre_carrera = forms.CharField(max_length=100)
    descripcion = forms.CharField()
    num_estudiante = forms.IntegerField()
    nombre_facultad = forms.CharField(max_length=100)
    fecha_apertura = forms.DateTimeField()