from django.db import models
from facultades.models import Facultad

class Carrera(models.Model):
    id_carrera = models.AutoField(primary_key=True,unique=True)
    nombre_carrera = models.CharField(max_length=100,blank=True,null=True)
    descripcion = models.TextField()
    num_estudiantes = models.PositiveIntegerField()
    nombre_facultad = models.ForeignKey(Facultad,blank=True,null=True)
    fecha_apertura = models.DateTimeField(auto_now=False)

    def __str__(self):
        return self.nombre_carrera

# Create your models here.
