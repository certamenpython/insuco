from django.shortcuts import render
from .forms import CarreForm
from .models import Carrera




def inicio(request):
    form = CarreForm(request.POST or None)

    if form.is_valid():
        form_data = form.cleaned_data
        nombre_carrera2 = form_data.get("nombre_carrera")
        descripcion2 = form_data.get("descripcion")
        num_estudiante2 = form_data.get("num_estudiante")
        nombre_facultad2 = form_data.get("nombre_facultad")
        fecha_apertura2 = form_data.get("fecha_apertura")
        objeto = Carrera.objects.create(nombre_carrera=nombre_carrera2,descripcion=descripcion2,num_estudiante=num_estudiante2,nombre_facultad=nombre_facultad2,fecha_apertura=fecha_apertura2)


    contexto = {
        "el_formulario": form
    }

    return render(request,"carrera.html",contexto)


# Create your views here.
