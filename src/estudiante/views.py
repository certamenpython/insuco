from django.shortcuts import render
from .forms import EstForm
from .models import Estudiante


def inicio(request):
    form = EstForm(request.POST or None)
    if form.is_valid():
        form_data = form.cleaned_data
        nombre2 = form_data.get("nombre")
        edad2 = form_data.get("edad")
        rut2 = form_data.get("rut")
        carrera2 = form_data.get("carrera")
        fecha_ingreso2 = form_data.get("fecha_ingreso")
        objecto = Estudiante.objects.create(nombre=nombre2,edad = edad2,rut=rut2,carrera=carrera2,fecha_ingreso=fecha_ingreso2)
    contexto = {
        "el_formulario" : form,
    }
    return render(request, "estudiante.html", contexto)




# Create your views here.
