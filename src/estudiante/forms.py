
from django import forms
from .models import Estudiante

class EstModelForm(forms.ModelForm):
    class Meta:
        modelo = Estudiante
        campos = {"nombre","edad","rut","carrera","fecha_ingreso"}



class EstForm(forms.Form):
    nombre = forms.CharField(max_length=100)
    edad = forms.IntegerField()
    rut = forms.IntegerField()
    carrera = forms.CharField(max_length=100)
    fecha_ingreso = forms.DateField()
