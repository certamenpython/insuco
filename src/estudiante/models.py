from django.db import models
from carrera.models import Carrera


class Estudiante(models.Model):
    id_estudiante = models.AutoField(primary_key=True,unique=True)
    nombre = models.CharField(max_length=100,blank=True,null=True)
    edad = models.PositiveIntegerField()
    rut = models.PositiveIntegerField()
    carrera = models.ForeignKey(Carrera,blank=True,null=True)
    fecha_ingreso = models.DateTimeField(auto_now=False)

    def __str__(self):
        return self.nombre



















# Create your models here.
