from django.contrib import admin
from estudiante.models import *
from .forms import EstForm,EstModelForm
class AdminEstudiante (admin.ModelAdmin):
    list_display = ["nombre","edad","rut","carrera","fecha_ingreso"]
    form = EstModelForm
    list_filter = ["fecha_ingreso"]
    list_editable = ["carrera"]
    search_fields = ["nombre","rut"]
    class Meta:
        model = Estudiante




admin.site.register(Estudiante,AdminEstudiante)
# Register your models here.
