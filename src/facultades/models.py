from django.db import models

class Facultad(models.Model):
    id_facultad = models.AutoField(primary_key=True,unique=True)
    nombre_facultad = models.CharField(max_length=100)
    descripcion = models.TextField(blank=True,null=True)
    ubicacion = models.CharField(blank=True,null=True,choices=(('CH','Chillán'),('C','Concepción'),('LA','Los Angeles')),max_length=100)
    num_departamento = models.PositiveIntegerField()

    def __str__(self):
        return self.nombre_facultad

# Create your models here.
