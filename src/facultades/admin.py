from django.contrib import admin
from facultades.models import *
from .forms import FacForm,FacModelForm


class AdminFacultad(admin.ModelAdmin):
    list_display = ["nombre_facultad","descripcion","ubicacion","num_departamento"]
    form = FacModelForm
    list_filter = ["ubicacion"]
    list_editable = ["descripcion"]
    search_fields = ["nombre_facultad","ubicacion"]

    class Meta:
        model = Facultad

admin.site.register(Facultad,AdminFacultad)
# Register your models here.
