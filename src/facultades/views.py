from django.shortcuts import render
from .forms import FacForm
from .models import Facultad



def inicio(request):
    form = FacForm (request.POST or None)

    if form.is_valid():
        form_data = form.cleaned_data
        nombre_facultad2 = form_data.get("nombre_facultad")
        descripcion2 = form_data.get("descripcion")
        ubicacion2 = form_data.get("ubicacion")
        num_departamento2 = form_data.get("num_departamento")
        objeto = Facultad.objects.create(nombre_facultad = nombre_facultad2,descripcion=descripcion2,ubicacion=ubicacion2,num_departamento=num_departamento2)

    contexto = {
        "el_formulario": form
    }


    return render(request,"facultad.html", contexto)



# Create your views here.
