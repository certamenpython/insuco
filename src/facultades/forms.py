from django import forms
from .models import Facultad

class FacModelForm(forms.ModelForm):
    class Meta:
        modelo = Facultad
        campos = ["nombre_facultad","descripcion","ubicacion","num_departamento"]


class FacForm(forms.Form):
    nombre_facultad = forms.CharField(max_length=100)
    descripcion= forms.CharField()
    ubicacion = forms.Select()
    num_departamento = forms.IntegerField()
